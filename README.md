# Object Detection with yoloV3

## Project Charta
![Project Charta](Planning/P_OD 04.10.20 Project Charta.PNG)

## ProductBreakdownStructure (PBS)
![PBS](Planning/P_OD 04.10.20 PBS.PNG)

## WorkBreakdownStructure (WBS)
![WBS](Planning/P_OD 04.10.20 WBS.PNG)

## Gantt Diagram
![Gantt Diagram](Planning/P_OD 04.10.20 Gantt.png)